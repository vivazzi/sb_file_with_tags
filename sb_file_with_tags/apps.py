from django.apps import AppConfig

from django.utils.translation import gettext_lazy as _


class SBFileWithTagsConfig(AppConfig):
    name = 'sb_file_with_tags'
    verbose_name = _('File (with tags)')
