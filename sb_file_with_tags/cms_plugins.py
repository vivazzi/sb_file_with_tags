from cms.plugin_pool import plugin_pool
from cms.plugin_base import CMSPluginBase
from file_resubmit.admin import AdminResubmitMixin

from sb_core.constants import BASE
from sb_file_with_tags.models import SBFileWithTags


class SBFileWithTagsPlugin(AdminResubmitMixin, CMSPluginBase):
    module = BASE
    model = SBFileWithTags
    name = 'Файл с тегами'
    render_template = 'sb_file/sb_file.html'
    text_enabled = True

    search_fields = ('title', 'link_title')


plugin_pool.register_plugin(SBFileWithTagsPlugin)
