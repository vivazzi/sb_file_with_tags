from django.db import models, migrations
import taggit.managers


class Migration(migrations.Migration):

    dependencies = [
        ('sb_file', '0001_initial'),
        ('taggit', '0002_auto_20150616_2121'),
    ]

    operations = [
        migrations.CreateModel(
            name='SBFileWithTags',
            fields=[
                ('sbfile_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='sb_file.SBFile', on_delete=models.CASCADE)),
                ('tags', taggit.managers.TaggableManager(to='taggit.Tag', through='taggit.TaggedItem', blank=True, help_text='A comma-separated list of tags.', verbose_name='Tags')),
            ],
            options={
                'db_table': 'sb_file_with_tags',
                'verbose_name': '\u0424\u0430\u0439\u043b \u0441 \u0442\u0435\u0433\u0430\u043c\u0438',
                'verbose_name_plural': '\u0424\u0430\u0439\u043b\u044b \u0441 \u0442\u0435\u0433\u0430\u043c\u0438',
            },
            bases=('sb_file.sbfile',),
        ),
    ]
