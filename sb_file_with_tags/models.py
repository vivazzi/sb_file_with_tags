from taggit.managers import TaggableManager
from sb_core.cms_models import FileModelPlugin, SBCMSPluginMixin
from sb_file.models import SBFile


class SBFileWithTags(SBFile):
    tags = TaggableManager(blank=True)

    def copy_relations(self, old_instance):
        self.tags.clear()
        for tag in old_instance.tags.all():
            self.tags.add(tag.name)

        SBCMSPluginMixin.copy_relations(self, old_instance)

    def __str__(self):
        if self.title: return self.title
        elif self.file: return self.get_filename()

        return '<пусто>'

    class Meta:
        db_table = 'sb_file_with_tags'
        verbose_name = 'Файл с тегами'
        verbose_name_plural = 'Файлы с тегами'
