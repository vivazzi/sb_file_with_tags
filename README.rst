=======
sb_file_with_tags
=======

sb_file_with_tags is django-cms plugin which is part of SBL (Service bussiness library, https://vits.pro/dev/).

This plugin adds file (with tags, that are saved in model) to page. sb_file_with_tags is wrapper of sb_file (https://bitbucket.org/vivazzi/sb_file/)


Installation
============

sb_file_with_tags requires sb_core package: https://bitbucket.org/vivazzi/sb_core/

There is no sb_file_with_tags in PyPI, so you can install this package from bitbucket repository only.

::
 
     $ pip install hg+https://bitbucket.org/vivazzi/sb_file_with_tags


Configuration 
=============

1. Add "sb_file_with_tags" to INSTALLED_APPS after "sb_core" ::

    INSTALLED_APPS = (
        ...
        'sb_core',
        'sb_file_with_tags',
        ...
    )

2. Run `python manage.py migrate` to create the sb_file_with_tags models.  
